setwd("/home/isma/Documents/sda/lab6-DM/phylogenetic-analysis")

library("msa")
library("seqinr")
library("phangorn")
library("UniProt.ws")
library("XML")
library("xml2")
library("stringr")

## We read the data
mySequences <- read.fasta("hemoglobin.fasta")

## We clean the Data
i <- 1
while (i <= 880) {
  
  data_xml <- read_xml(x = paste("https://www.uniprot.org/uniprot/",names(mySequences)[i],".xml",sep=""), as_html = TRUE)
  
  word = str_split(as_list(xml_child(xml_child(xml_child(xml_child(xml_child(data_xml,"body"),"uniprot"),"entry"),"organism"),"name"))[[1]],pattern = " ")
  genus = str_trunc(word[[1]][1],width = 1, ellipsis = "")
  species  = str_trunc(word[[1]][2],width = 3, ellipsis = "")
  wordfinal = paste(genus,species,sep="") 
  
  datasummary <- paste(as_list(xml_child(xml_child(xml_child(xml_child(data_xml,"body"),"uniprot"),"entry"),"name"))[[1]],
                       wordfinal,
                       as_list(xml_child(xml_child(xml_child(xml_child(xml_child(data_xml,"body"),"uniprot"),"entry"),"organism"),"lineage"))[[7]],
                       as_list(xml_child(xml_child(xml_child(xml_child(data_xml,"body"),"uniprot"),"entry"),"accession"))[[1]],
                       sep = "-")
    
  names(mySequences)[i] <- datasummary
  i <- (i + 1)
}
remove(i)


write.fasta(mySequences, names(mySequences), "cleanHemoglobin.fasta", open = "w", nbchar = 60, as.string = FALSE)

mySequences <- read.fasta("cleanHemoglobin.fasta")

## We create the subsets

mySubset <- function(size,mySequences){
  subset <- mySequences[c(52,1,2,3,4,8,13,15,221,232,814,318,693)]
  picked_number <- c(52,1,2,3,4,8,13,15,221,232,814,318,693)
  i <- 14
  while (i <= size) {
    ## pick a random number between 5 and 880 
    j <- sample(5:880, 1)
    
    ## check  it is not equal to one we already choose
    while(j %in% picked_number || length(mySequences[[names(mySequences)[j]]])>150 ||  length(mySequences[[names(mySequences)[j]]])<130){
      j <-  sample(5:880, 1)
    }
    
    ## add it into the subset
    subset[names(mySequences)[j]] <- mySequences[j]
    
    ## add it into the list of already choosen number 
    picked_number[i] <- j
    
    i <- (i + 1)
  }
  return(subset)
}
                    
subsetA <- mySubset(100,mySequences)
subsetB <- mySubset(50,mySequences)  
subsetC <- mySubset(20,mySequences)

write.fasta(subsetA, names(subsetA), "subsetA.fasta", open = "w", nbchar = 60, as.string = FALSE)
write.fasta(subsetB, names(subsetB), "subsetB.fasta", open = "w", nbchar = 60, as.string = FALSE)
write.fasta(subsetC, names(subsetC), "subsetC.fasta", open = "w", nbchar = 60, as.string = FALSE)


## We analyse the subsets

# alignement =  c("ClustalW", "Muscle", "ClustalOmega") , matrix = c("hamming","ml")
#analysisi_phylogenetic <- function(subset, alignement, matrix){
  
  ## create the alignement
  myAlignement <- msa(paste(subset,"fasta",sep = "."), method = alignement, type = "protein")
  
  ## print the alignement in the directory in pdf
  msaPrettyPrint(myAlignement, output="pdf", showNames="left",showLogo="none", askForOverwrite=FALSE, verbose=FALSE)
  
  ## store alignement in phyDat format 
  myphydat <-  phangorn::as.phyDat(myAlignement, type = "AA",names = names(subset) )
  names(myphydat) <- names(subset)
  
  ## calculate and save distance matrix 
  distance = NULL
  if(matrix =="hamming"){
    distance <- dist.hamming(myphydat)
  }
  else{
    distance <- dist.ml(myphydat)
  }
  
  save(distance, file = paste("distance",alignement,matrix,".Rdata",sep=""))
  
  ## analyse the data using different methods
  
  # - UPGMA -  https://en.wikipedia.org/wiki/UPGMA  rooted tree
  upgma <- upgma(distanceMatrixHamming, method = "average")
  
  # - NJ  - https://en.wikipedia.org/wiki/Neighbor_joining
  nj <- NJ(distance)
  
  # - PARSIMONY -  https://en.wikipedia.org/wiki/Maximum_parsimony_(phylogenetics)
  parsimonynj <- parsimony(nj, myphydat)
  parsimonyupgma <-  parsimony(upgma, myphydat)

  # MINIMUM EVOLUTION 
  meh <- fastme.bal(distance, nni = TRUE, spr = TRUE, tbr = TRUE) #balanced
  mefmh <- fastme.ols(distance, nni = TRUE) #ols

  # MODELTEST function with a AIC, AICc and BIC (phangorn)
  modeltest <- modelTest(myphydat, tree = mefmh, model = c("all"))
  
  return(upgma)
}

## Do some analysis and plot trees : 

## We are going to this analysis with the subsetC because it is the easier to work with but you can change this parameter here
subsetW <- subsetA
subsetWs <- "subsetA"

## Alignement 

myClustalWAlignement <- msa(paste(subsetWs,"fasta",sep = "."), method = "ClustalW", type = "protein")
myMuscleAlignement <- msa(paste(subsetWs,"fasta",sep = "."), method = "Muscle", type = "protein")
myClustalOmegaAlignement <- msa(paste(subsetWs,"fasta",sep = "."), method = "ClustalOmega", type = "protein")

msaPrettyPrint(myClustalWAlignement, output="pdf", showNames="left",
               showLogo="none", askForOverwrite=FALSE, verbose=FALSE)


msaPrettyPrint(myMuscleAlignement, output="pdf", showNames="left",
               showLogo="none", askForOverwrite=FALSE, verbose=FALSE)


msaPrettyPrint(myClustalOmegaAlignement, output="pdf", showNames="left",
               showLogo="none", askForOverwrite=FALSE, verbose=FALSE)

## make distance matrices (and store them in separate files)

# for ClustalW
myphydat <-  phangorn::as.phyDat(myClustalWAlignement, type = "AA",names = names(subsetW) )
names(myphydat) <- names(subsetW)

distanceMatrixHamming <- dist.hamming(myphydat)
distanceml <- dist.ml(myphydat)

# for ClustalOmega
save(distanceMatrixHamming, file = "distancematrix.Rdata")
save(distanceml, file = "distanceml.Rdata")

myphydat2 <-  phangorn::as.phyDat(myClustalOmegaAlignement, type = "AA",names = names(subsetW) )
names(myphydat2) <- names(subsetW)

distanceMatrixHamming2 <- dist.hamming(myphydat2)
distanceml2 <- dist.ml(myphydat2)

save(distanceMatrixHamming2, file = "distancematrixO.Rdata")
save(distanceml2, file = "distancemlO.Rdata")

## ALGORITHMS

# - UPGMA -  https://en.wikipedia.org/wiki/UPGMA  rooted tree
upgmaH <- upgma(distanceMatrixHamming, method = "average")
upgmaML <- upgma(distanceml, method = "average")

upgmaH2 <- upgma(distanceMatrixHamming2, method = "average")
upgmaML2 <- upgma(distanceml2, method = "average")


# - NJ  - https://en.wikipedia.org/wiki/Neighbor_joining
njH <- NJ(distanceMatrixHamming)
njL <- NJ(distanceml)

njH2 <- NJ(distanceMatrixHamming2)
njL2 <- NJ(distanceml2)

# MINIMUM EVOLUTION

# Clustal W
meh <- fastme.bal(distanceMatrixHamming, nni = TRUE, spr = TRUE, tbr = TRUE) #balanced
meml <- fastme.bal(distanceml, nni = TRUE, spr = TRUE, tbr = TRUE)
mefmh <- fastme.ols(distanceMatrixHamming, nni = TRUE) #ols
mefml <- fastme.ols(distanceml, nni = TRUE)

# Clustal Omega

meh2 <- fastme.bal(distanceMatrixHamming2, nni = TRUE, spr = TRUE, tbr = TRUE) #balanced
meml2 <- fastme.bal(distanceml2, nni = TRUE, spr = TRUE, tbr = TRUE)
mefmh2 <- fastme.ols(distanceMatrixHamming2, nni = TRUE) #ols
mefml2 <- fastme.ols(distanceml2, nni = TRUE)

# - PARSIMONY -  https://en.wikipedia.org/wiki/Maximum_parsimony_(phylogenetics)
p1 <- parsimony(njH, myphydat)
p2 <- parsimony(njL, myphydat)
p3 <- parsimony(njH2, myphydat2)
p4 <- parsimony(njL2, myphydat2)
p5 <- parsimony(upgmaH, myphydat)
p6 <- parsimony(upgmaML, myphydat)
p7 <- parsimony(upgmaH2, myphydat2)
p8 <- parsimony(upgmaML2, myphydat2)
p9 <- parsimony(meh, myphydat)
p10 <- parsimony(meml, myphydat)
p11 <- parsimony(mefmh, myphydat)
p12 <- parsimony(mefml, myphydat)
p13 <- parsimony(meh2, myphydat2)
p14 <- parsimony(meml2, myphydat2)
p15 <- parsimony(mefmh2, myphydat2)
p16 <- parsimony(mefml2, myphydat2)

# modelTest function with a AIC, AICc and BIC (phangorn)

modeltest <- modelTest(myphydat, tree = mefmh, model = c("Dayhoff"))


## bootstraping 

BStrees <- bootstrap.phyDat(myphydat,pratchet,bs = 100)

treeMP <- pratchet(myphydat)
treeMP <- acctran(treeMP, myphydat)

treeMP <- plotBS(treeMP, BStrees, type ="phylogram")



## VISUALISATION 

plot.phylo(upgmaH, main ="upgma (Hamming) (ClustalW)")
plot.phylo(upgmaML, main ="UPGMA (ML) (ClustalW)")
plot.phylo(upgmaH2, main ="upgmaH2")

write.nexus(upgmaH, file = "upgmaH.nex")bootstrap.pml

write.nexus(upgmaML, file = "upgmaML.nex")
write.nexus(upgmaH2, file = "upgmaH2.nex")

plot.phylo(njH, main = "NJH", type ="unrooted", x.lim = 1000 , y.lim = 500 )
plot.phylo(njL, type ="unrooted", lab4ut = "axial")
plot(njL)

write.nexus(njH, file = "njH.nex")
write.nexus(njL, file = "njL.nex")

plot.phylo(meh,main = "meh")
plot.phylo(meml, type ="unrooted", lab4ut = "axial" )
plot.phylo(mefmh, main = "mefmh")
plot.phylo(mefml, main = "mefml")

write.nexus(meh, file = "meh.nex")
write.nexus(meml, file = "meml.nex")
write.nexus(mefmh, file = "mefmh.nex")
write.nexus(mefml, file = "mefml.nex")




